# Bartosz opencart-testobject

Een virtual box image met de Bitnami OpenCart webshop

### Gebruik:
1. Installeer als je het nog niet gedaan hebt [VirtualBox](https://www.virtualbox.org)
2. Importeer het image uit deze repository, zie de map [ova](https://gitlab.com/bartoszict/opencart-testobject/tree/master/ova)
3. Gebruik de volgende `Applicance settings`
	![](doc/img/opencartvm_import_settings.png "Applicance settings")
4. De VM is nu beschikbaar in de VirtualBox interface
  ![](doc/img/opencartvm_imported.png "Imported")
5. Om op een gemakkelijke manier bij de webshop (webinterface en database) kun je de netwerk adapter instellen als `Bridged Adapter`.
Dit doe je via de optie `Settings`, tabblad `Network`
  ![](doc/img/opencartvm_network_bridged.png "Running opencart")
6. Klik op `Start`
7. De VM start en er verschijnt een scherm (
  ![](doc/img/opencartvm_started.png "Running opencart")
8. Noteer het IP adres en ga er naar toe in de browser, je kunt nu via de interface een account aanmaken.
9. Voor database toegang gebruik je je eigen favoriete tooling (bijvoorbeeld [DBeaver](https://dbeaver.io)) met deze settings
		
		Database driver = MySQL 8+
		Server host = het IP adres van stap 8
		Port = 3306
		Username = bartosz
		Password = bartosz
		
		Je ziet een schema `bitnami_opencart` met alle beschikbare tabellen.
		Met `select * from bitnami_opencart.oc_customer;` kun je je zojuist aangemaakte user vinden in de database.
		
	